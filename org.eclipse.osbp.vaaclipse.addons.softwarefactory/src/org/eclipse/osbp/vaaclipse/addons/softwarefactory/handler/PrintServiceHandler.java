/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.handler;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.utils.themes.ui.VaaclipseUiTheme;
import org.eclipse.osbp.utils.themes.ui.VaaclipseUiTheme.ThemeList;
import org.eclipse.osbp.vaaclipse.api.ResourceInfoProvider;
import org.eclipse.osbp.vaaclipse.publicapi.theme.Theme;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeConstants;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeEngine;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeManager;
import org.eclipse.osbp.xtext.reportdsl.common.PdfPrintService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedHttpSession;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.FormLayout;

@SuppressWarnings("all")
public class PrintServiceHandler implements IUser.UserLocaleListener {
	private final static Logger log = LoggerFactory.getLogger("change theme");
	@Inject
	private IEventBroker eventBroker;

	private ComboBox printServiceCombo;

	@Inject
	private IUser user;

	/** The metadata service. */
	@Inject
	private IDSLMetadataService dslMetadataService;

	@PostConstruct
	public void init(final ComponentContainer container) {
		FormLayout form = new FormLayout();
		form.addStyleName("ToolControlCombo");
		form.setMargin(false);
		form.setSpacing(false);
		printServiceCombo = new ComboBox();
		printServiceCombo.setItemCaptionMode(ItemCaptionMode.EXPLICIT_DEFAULTS_ID);
		printServiceCombo.setNullSelectionAllowed(false);
		printServiceCombo.setWidth("200px");
		printServiceCombo.setTextInputAllowed(false);
		reloadCombo();
		form.addComponent(printServiceCombo);
		container.addComponent(form);
		printServiceCombo.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				String printService = ((String) event.getProperty().getValue());
				if (log.isDebugEnabled())
					log.debug("will set print service to:" + printService);
				user.setPrintService(printService);
			}
		});
		user.addUserLocaleListener(this);
	}

	protected void reloadCombo() {
		String[] printServices = PdfPrintService.reLookupPrintServiceNames().toArray(new String[0]);
		printServiceCombo.removeAllItems();
		for (String printService : printServices) {
			try {
				printServiceCombo.addItem(printService);
			} catch (Exception e) {
			}
		}
		printServiceCombo.setPageLength(printServices.length);
		if(user.getPrintService() != null) {
			printServiceCombo.select(user.getPrintService());
		} else if (printServices.length > 0){
			printServiceCombo.select(printServices[0]);
		}
	}
	
	@Override
	public void localeChanged(Locale locale) {
		printServiceCombo.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "printServiceTooltip"));
	}
}
