/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.handler;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.utils.themes.ui.VaaclipseUiTheme;
import org.eclipse.osbp.utils.themes.ui.VaaclipseUiTheme.ThemeList;
import org.eclipse.osbp.vaaclipse.api.ResourceInfoProvider;
import org.eclipse.osbp.vaaclipse.publicapi.theme.Theme;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeConstants;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeEngine;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedHttpSession;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.FormLayout;

@SuppressWarnings("all")
public class ThemeHandler implements IUser.UserLocaleListener {
	private final static Logger log = LoggerFactory.getLogger("change theme");
	@Inject
	private IEventBroker eventBroker;

	@Inject
	private ThemeEngine themeEngine;

	@Inject
	private ThemeManager themeManager;

	@Inject
	private ResourceInfoProvider resourceInfoProvider;

	private ComboBox themeCombo;

	private List<String> themeBlackList;

	@Inject
	private IThemeResourceService themeResourceService;

	@Inject
	private IUser user;

	/** The metadata service. */
	@Inject
	private IDSLMetadataService dslMetadataService;

	@PostConstruct
	public void init(final ComponentContainer container) {
		FormLayout form = new FormLayout();
		form.addStyleName("ToolControlCombo");
		form.setMargin(false);
		form.setSpacing(false);
		themeCombo = new ComboBox();
		themeCombo.setItemCaptionMode(ItemCaptionMode.EXPLICIT_DEFAULTS_ID);
		themeCombo.setNullSelectionAllowed(false);
		themeCombo.setWidth("200px");
		themeCombo.setTextInputAllowed(false);
		VaaclipseUiTheme actualTheme = null;

		// get a preset theme or the default
		HttpSession httpSession = ((WrappedHttpSession) VaadinSession.getCurrent().getSession()).getHttpSession();
		String actualThemeId = (String) httpSession.getAttribute(ThemeConstants.Attrubutes.themeid);
		if (actualThemeId == null) {
			actualThemeId = resourceInfoProvider.getCssTheme();
		}

		Set<VaaclipseUiTheme> themes = VaaclipseUiTheme.values(ThemeList.forProduct);
		for (VaaclipseUiTheme theme : themes) {
			try {
				themeCombo.addItem(theme);
				themeCombo.setItemCaption(theme, theme.getLabel());
				if (theme.getId().equals(actualThemeId)) {
					actualTheme = theme;
				}
			} catch (Exception e) {
			}
		}
		themeCombo.setPageLength(themes.size());
		form.addComponent(themeCombo);
		container.addComponent(form);
		themeCombo.select(actualTheme);
		themeCombo.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (log.isDebugEnabled())
					log.debug("will set theme to:" + event.getProperty().getValue());
				VaaclipseUiTheme uiTheme = ((VaaclipseUiTheme) event.getProperty().getValue());
				Theme theme = themeEngine.getTheme(uiTheme.getId());
				if ((theme != null) && (uiTheme.getId() != null)) {
					if (themeCombo.getUI() != null) {
						themeCombo.getUI().setTheme(uiTheme.getId());
					}
					if (themeManager != null) {
						themeManager.setTheme(uiTheme.getId());
					}
					eventBroker.send(ThemeConstants.Events.setThemeEvent, theme);
					themeResourceService.reset();
				}
			}
		});
		user.addUserLocaleListener(this);
	}

	@Override
	public void localeChanged(Locale locale) {
		themeCombo.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "themesTooltip"));
	}
}
