/*
 *                                                                            
 *  Copyright (c) 2011 - 2017 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                              
 * 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.administration;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.osbp.ui.api.contextfunction.IVaadinDialogProvider;
import org.osgi.service.component.annotations.Component;

/**
 * The Class KeyBindingDialogContextFunction.
 */
@Component(service = IContextFunction.class, property = "service.context.key=ReportPrinterDialog")
@SuppressWarnings("all")
public class ReportPrinterDialogContextFunction implements IContextFunction {

	/* (non-Javadoc)
	 * @see org.eclipse.e4.core.contexts.IContextFunction#compute(org.eclipse.e4.core.contexts.IEclipseContext, java.lang.String)
	 */
	@Override
	public Object compute(IEclipseContext context, String contextKey) {
	    MApplication application = context.get(MApplication.class);
	    IEclipseContext appCtx = application.getContext();
	    IVaadinDialogProvider provider = ContextInjectionFactory.make(ReportPrinterDialogProvider.class, appCtx);
	    appCtx.set(IVaadinDialogProvider.class, provider);
	    return provider;
	}

}
