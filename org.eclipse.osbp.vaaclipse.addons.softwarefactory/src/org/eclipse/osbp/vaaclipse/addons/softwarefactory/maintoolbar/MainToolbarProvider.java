/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.maintoolbar;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.commands.MCommand;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimBar;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimElement;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.menu.ItemType;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarSeparator;
import org.eclipse.e4.ui.model.application.ui.menu.impl.MenuFactoryImpl;
import org.eclipse.osbp.ui.api.perspective.IPerspectiveProvider;
import org.eclipse.osbp.ui.api.themes.EnumCssClass;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType;
import org.eclipse.osbp.vaaclipse.presentation.engine.GenericPresentationEngine;
import org.eclipse.osbp.xtext.action.ActionButton;
import org.eclipse.osbp.xtext.action.ActionSpacer;
import org.eclipse.osbp.xtext.action.ActionToolbar;
import org.eclipse.osbp.xtext.action.ActionToolbarItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;

/**
 * The Class MainToolbarProvider.
 */
@SuppressWarnings("restriction")
public class MainToolbarProvider implements IMainToolbarProvider {

	private static final String PERSPECTIVE_ID = "perspectiveId";

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(MainToolbarProvider.class);

	/** The app model. */
	@Inject
	private MApplication appModel;

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	/** The theme resource service. */
	@Inject
	private IThemeResourceService themeResourceService;

	/** The trim bar. */
	private MTrimBar trimBar;

	/** The menu factory. */
	private MenuFactoryImpl menuFactory;

	/** The presentation engine. */
	private GenericPresentationEngine presentationEngine;

	/** The unique toolbar id. */
	private String toolbarId;

	/**
	 * Instantiates a new main toolbar provider. It is injected later via
	 * E4ContextFunction.
	 */
	@Inject
	public MainToolbarProvider() { // NOSONAR
	}

	/**
	 * Inits the maintoolbar provider.
	 */
	@PostConstruct
	public void init() {
		log.debug("init MainToolbarProvider");
		menuFactory = MenuFactoryImpl.init();
		presentationEngine = eclipseContext.get(GenericPresentationEngine.class);
		trimBar = ((MTrimmedWindow) appModel.getChildren().get(0)).getTrimBars().get(1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.vaaclipse.addons.softwarefactory.maintoolbar.
	 * IMainToolbarProvider#addToolbar(java.lang.String,
	 * org.eclipse.osbp.xtext.action.ActionToolbar, java.lang.String)
	 */
	@Override
	public void addToolbar(String perspectiveId, ActionToolbar toolBarModel) {
		log.debug("addToolbar for perspectiveId {}", perspectiveId);
		MToolBar toolBar = createToolbar(perspectiveId, toolBarModel);
		createToolbarArea(toolBar);
	}

	/**
	 * Creates the toolbar area.
	 *
	 * @param toolBar
	 *            the tool bar
	 */
	void createToolbarArea(MToolBar toolBar) {
		// set os style
		CssLayout toolBarArea = (CssLayout) trimBar.getWidget();
		Component toolBarWidget = null;
		for (int idx = 0; idx < toolBarArea.getComponentCount(); idx++) {
			Component test = toolBarArea.getComponent(idx);
			AbstractLayout hl = (AbstractLayout) test;
			if (hl.getData() instanceof MToolBar) {
				toolBarWidget = toolBarArea.getComponent(idx);
				break;
			}
		}
		if (toolBarWidget == null) {
			// create toolBar
			toolBarWidget = (Component) presentationEngine.createGui(toolBar);
			((AbstractLayout) toolBarWidget).setSizeUndefined();
		}
		toolBarWidget.addStyleName(EnumCssClass.MPARTTOOLBAR.toString());
		toolBarArea.addComponentAsFirst(toolBarWidget);
	}

	/**
	 * Creates the toolbar.
	 *
	 * @param perspectiveId
	 *            the perspective id
	 * @param toolBarModel
	 *            the tool bar model
	 * @param actionUri
	 *            the action uri
	 * @return the m tool bar
	 */
	MToolBar createToolbar(String perspectiveId, ActionToolbar toolBarModel) {
		MToolBar toolBar = null;
		toolBar = menuFactory.createToolBar();
		toolBar.setElementId(IPerspectiveProvider.E4Constants.TOOLBARCONTROLS + "." + perspectiveId);
		toolBar.setToBeRendered(true);
		eclipseContext.set("TOOLBAR_UUID", toolBar.getElementId());
		addToolbarItems(toolBar, toolBarModel);
		// set handle
		toolBar.getTransientData().put(PERSPECTIVE_ID, perspectiveId);
		trimBar.getChildren().add(toolBar);
		return toolBar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.vaaclipse.addons.softwarefactory.maintoolbar.
	 * IMainToolbarProvider#removeToolbar(java.lang.String)
	 */
	@Override
	public void removeToolbar(String perspectiveId) {
		log.debug("removeToolbar for perspectiveId {}", perspectiveId);
		MTrimElement elementToRemove = null;
		for (MTrimElement element : trimBar.getChildren()) {
			if (element instanceof MToolBar
					&& perspectiveId.equals(((MToolBar) element).getTransientData().get(PERSPECTIVE_ID))) {
				removeToolbarItems((MToolBar) element);
				elementToRemove = element;
			}
		}
		trimBar.getChildren().remove(elementToRemove);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.vaaclipse.addons.softwarefactory.maintoolbar.
	 * IMainToolbarProvider#setActive(java.lang.String, boolean)
	 */
	@Override
	public void setActive(String perspectiveId, boolean active) {
		for (MTrimElement element : trimBar.getChildren()) {
			if (element instanceof MToolBar
					&& perspectiveId.equals(((MToolBar) element).getTransientData().get(PERSPECTIVE_ID))) {
				((MToolBar) element).setVisible(active);
			}
		}
	}

	/**
	 * Add toolBar items.
	 *
	 * @param toolBar
	 *            the tool bar
	 * @param toolBarModel
	 *            the tool bar model
	 * @param actionUri
	 *            the action uri
	 */
	private void addToolbarItems(MToolBar toolBar, ActionToolbar toolBarModel) {
		for (ActionToolbarItem buttonModel : toolBarModel.getActions()) {
			addToolbarItem(buttonModel, toolBar);
		}
	}

	/**
	 * Adds the toolbar item.
	 *
	 * @param itemModel
	 *            the tool bar item model
	 * @param toolBar
	 *            the tool bar
	 * @param actionUri
	 *            the action uri
	 */
	private void addToolbarItem(ActionToolbarItem itemModel, MToolBar toolBar) {
		if (itemModel instanceof ActionButton) {
			ActionButton buttonModel = (ActionButton)itemModel;
			MHandledToolItem toolItem = menuFactory.createHandledToolItem();
			toolItem.setType(ItemType.PUSH);

			toolItem.setTooltip(buttonModel.getName());
			toolItem.setIconURI(themeResourceService.getThemeURI(buttonModel.getCommand().getIconURI(), ThemeResourceType.ICON));
			toolItem.setEnabled(true);
			toolItem.setToBeRendered(true);
			toolItem.setVisible(true);

			setCommand(buttonModel.getCommand().getName(), toolItem, appModel);
			toolItem.setElementId(IPerspectiveProvider.E4Constants.TOOLBARITEM_PREFIX + buttonModel.getName());
			toolBar.getChildren().add(toolItem);
		}else if(itemModel instanceof ActionSpacer){
			MToolBarSeparator separatorItem = menuFactory.createToolBarSeparator();
			separatorItem.setToBeRendered(true);
			separatorItem.setVisible(true);
			separatorItem.setElementId(IPerspectiveProvider.E4Constants.TOOLBARITEM_PREFIX + "_SEPARATOR_" + String.valueOf(toolBar.getChildren().size()));

			toolBar.getChildren().add(separatorItem);
		}
	}

	private void setCommand(final String commandName, final MHandledToolItem toolItem, final MApplication application) {
		for (MCommand command : application.getCommands()) {
			if (command.getCommandName().equals(commandName)) {
				toolItem.setCommand(command);
				break;
			}
		}

	}

	/**
	 * Removes toolBar items.
	 *
	 * @param toolBar
	 *            the tool bar
	 */
	private void removeToolbarItems(MToolBar toolBar) {
		toolBar.getChildren().clear();
	}

	@Override
	public String getToolbarId() {
		return toolbarId;
	}

}
