/*
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                               
 * 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.userfilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.authentication.account.dtos.FilterDto;
import org.eclipse.osbp.authentication.account.dtos.UserGroupDto;
import org.eclipse.osbp.authentication.account.dtos.mapper.FilterDtoMapper;
import org.eclipse.osbp.authentication.account.dtos.mapper.UserGroupDtoMapper;
import org.eclipse.osbp.authentication.account.entities.Filter;
import org.eclipse.osbp.authentication.account.entities.UserGroup;
import org.eclipse.osbp.core.api.persistence.IPersistenceService;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;
import org.eclipse.osbp.jpa.services.Query;
import org.eclipse.osbp.jpa.services.filters.LCompare;
import org.eclipse.osbp.jpa.services.filters.LCompare.Equal;
import org.eclipse.osbp.jpa.services.filters.LJoinFilter;
import org.eclipse.osbp.runtime.common.annotations.DtoUtils;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.common.types.IBundleSpace;
import org.eclipse.osbp.runtime.web.vaadin.common.data.BeanServiceLazyLoadingContainer;
import org.eclipse.osbp.runtime.web.vaadin.common.data.IAlreadyLoadedItemIdProvider;
import org.eclipse.osbp.runtime.web.vaadin.common.data.StatefulDelegatingDtoSearchService;
import org.eclipse.osbp.ui.api.contextfunction.IVaadinDialogProvider;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.utils.common.EntityUtils;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.eclipse.osbp.vaadin.emf.data.fields.EObjectComboBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class UserFilterDialogProvider enables to inject a filter on runtime data
 * into the dto service.
 */
@SuppressWarnings("restriction")
public class UserFilterDialogProvider implements IVaadinDialogProvider, ValueChangeListener, IUser.UserLocaleListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The persistence service. */
	@Inject
	private IPersistenceService persistenceService;

	/** The bundle space. */
	@Inject
	private IBundleSpace bundleSpace;

	/** The modeling context. */
	@Inject
	IModelingContext modelingContext;

	/** The user field. */
	private ComboBox userGroupField;

	/** The entity field. */
	private EObjectComboBox entityField;

	/** The filter field. */
	private ListSelect filterField;

	/** The filter select all. */
	private Button filterSelectAll;

	/** The filter select none. */
	private Button filterSelectNone;

	/** The save. */
	private Button save;

	/** The filter check invers. */
	private CheckBox filterCheckInvers;

	/** The filter delimiter. */
	private static final String FILTER_DELIMITER = ":";

	/** The persistence id. */
	private String persistenceId = "authentication";

	/** The emf. */
	private transient EntityManagerFactory emf;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserFilterDialogProvider.class);

	/** The metadata service. */
	@Inject
	private IDSLMetadataService dslMetadataService;

	/** The user. */
	@Inject
	private IUser user;

	/**
	 * The listener interface for receiving userComboBoxValueChange events. The
	 * class that is interested in processing a userComboBoxValueChange event
	 * implements this interface, and the object created with that class is
	 * registered with a component using the component's
	 * <code>addUserComboBoxValueChangeListener<code> method. When the
	 * userComboBoxValueChange event occurs, that object's appropriate method is
	 * invoked.
	 *
	 * @see UserComboBoxValueChangeEvent
	 */
	private class UserComboBoxValueChangeListener implements ValueChangeListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin
		 * .data.Property.ValueChangeEvent)
		 */
		@Override
		public void valueChange(ValueChangeEvent event) {
			entityField.clear();
			filterField.setContainerDataSource(null);
			filterSelectAll.setEnabled(false);
			filterSelectNone.setEnabled(false);
			filterCheckInvers.clear();
		}
	}

	/**
	 * The listener interface for receiving entityComboBoxValueChange events.
	 * The class that is interested in processing a entityComboBoxValueChange
	 * event implements this interface, and the object created with that class
	 * is registered with a component using the component's
	 * <code>addEntityComboBoxValueChangeListener<code> method. When the
	 * entityComboBoxValueChange event occurs, that object's appropriate method
	 * is invoked.
	 *
	 * @see EntityComboBoxValueChangeEvent
	 */
	private class EntityComboBoxValueChangeListener implements ValueChangeListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin
		 * .data.Property.ValueChangeEvent)
		 */
		@Override
		public void valueChange(ValueChangeEvent event) {
			filterField.setContainerDataSource(null);
			LEntity entityFieldValue = (LEntity) entityField.getValue();
			if (entityFieldValue != null) {
				for (LEntityAttribute entityAttr : entityFieldValue.getAllAttributes()) {
					if (entityAttr.isDomainKey()) {
						filterField.setItemCaptionPropertyId(entityAttr.getName());
						fillfilterComboBox(entityFieldValue);
					}
				}
			}
		}

		/**
		 * Fillfilter combo box.
		 *
		 * @param entity
		 *            the entity
		 */
		private void fillfilterComboBox(LEntity entity) {
			String fqnDto = EntityUtils.getDtoFQNForLEntity(entity);
			try {
				@SuppressWarnings("unchecked")
				Class<IDto> dtoClass = (Class<IDto>) bundleSpace.forName(fqnDto);
				IDTOService<IDto> service = DtoServiceAccess
						.getService(dtoClass);
				StatefulDelegatingDtoSearchService<IDto> loadingService = new StatefulDelegatingDtoSearchService<>(
						service, dtoClass);
				BeanServiceLazyLoadingContainer<? extends IDto> datasource = new BeanServiceLazyLoadingContainer<>(
						loadingService, dtoClass, null);
				int size1 = filterField.getItemIds().size();
				filterField.setContainerDataSource(datasource);
				int size2 = filterField.getItemIds().size();
				if (size1 == 0 && size2 > 0) {
					filterSelectAll.setEnabled(true);
					filterSelectNone.setEnabled(true);
				}
				// optional preselect
				preSelectFilter();
			} catch (ClassNotFoundException e) {
				LOGGER.error("{}", e);
			}
		}

		/**
		 * Pre select filter.
		 */
		private void preSelectFilter() {
			// New empty list for the old selected and persisted filters that
			// will
			// be preselected.
			final Set<IDto> selectedFilters = new HashSet<>();
			// Searching for candidates for preselection
			for (FilterDto accountFilterDto : getFilters()) {
				String filter = accountFilterDto.getFilter();
				String filterId = getFilterId(filter);
				String filterEntity = getFilterEntity(filter);
				@SuppressWarnings("unchecked")
				Collection<IDto> items = (Collection<IDto>) filterField.getContainerDataSource().getItemIds();
				String entityDtoName = EntityUtils.getQualifiedDtoNameForQualifiedEntityName(filterEntity);
				for (IDto dto : items) {
					// Excluding not existing entities within the filters in
					// Filter
					if (dto.getClass().getName().equals(entityDtoName)) { // NOSONAR
						// selection of the inverse check-box
						boolean inversValue = accountFilterDto.getInvers();
						if (filterCheckInvers.getValue() != inversValue) {
							filterCheckInvers.setValue(inversValue);
						}
						// Identify the entity candidates comparing their ID
						// with
						// the persisted ID within the filter in
						// Filter
						Object idValue = DtoUtils.getIdValue(dto);
						if (idValue instanceof Integer) {
							if (new Integer(filterId).equals(idValue)) {
								selectedFilters.add(dto);
							}
						} else {
							if (filterId.equals(idValue)) {
								selectedFilters.add(dto);
							}
						}
					}
				}
			}
			// selection of the filter
			filterField.setValue(selectedFilters);
		}

		/**
		 * Gets the filter id.
		 *
		 * @param filter
		 *            the filter
		 * @return the filter id
		 */
		private String getFilterId(String filter) {
			String[] splittedFilter = filter.split(FILTER_DELIMITER);
			if (splittedFilter.length > 1) {
				return splittedFilter[1];
			}
			return "";
		}

		/**
		 * Gets the filter entity.
		 *
		 * @param filter
		 *            the filter
		 * @return the filter entity
		 */
		private String getFilterEntity(String filter) {
			String[] splittedFilter = filter.split(FILTER_DELIMITER);
			if (splittedFilter.length > 0) {
				return splittedFilter[0];
			}
			return "";
		}

		/**
		 * Gets the filters.
		 *
		 * @return the filters
		 */
		private Collection<FilterDto> getFilters() {
			// UserAccount
			UserGroupDto userGroup = (UserGroupDto) userGroupField.getValue();
			// create UserAccountDTOService
			IDTOService<FilterDto> service = DtoServiceAccess
					.getService(FilterDto.class);
			// find all filter elements with the reference to the
			// providing usergroup id.
			if (userGroup != null) {
				Equal idCompareFilter = new LCompare.Equal("id", userGroup.getId());
				LJoinFilter filter = new LJoinFilter("userGroup", idCompareFilter);
				return service.find(new Query(filter));
			}
			return Collections.emptyList();
		}

	}

	/**
	 * The listener interface for receiving filterValueChange events. The class
	 * that is interested in processing a filterValueChange event implements
	 * this interface, and the object created with that class is registered with
	 * a component using the component's
	 * <code>addFilterValueChangeListener<code> method. When the
	 * filterValueChange event occurs, that object's appropriate method is
	 * invoked.
	 *
	 * @see FilterValueChangeEvent
	 */
	private class FilterValueChangeListener implements ValueChangeListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin
		 * .data.Property.ValueChangeEvent)
		 */
		@Override
		public void valueChange(ValueChangeEvent event) {
			@SuppressWarnings("unchecked")
			Collection<IDto> value = (Collection<IDto>) filterField.getValue();
			int size = value.size();
			if (size > 0 && size < filterField.getItemIds().size()) {
				filterCheckInvers.setEnabled(true);
			}
		}
	}

	/**
	 * The listener interface for receiving filterSelectAllClick events. The
	 * class that is interested in processing a filterSelectAllClick event
	 * implements this interface, and the object created with that class is
	 * registered with a component using the component's
	 * <code>addFilterSelectAllClickListener<code> method. When the
	 * filterSelectAllClick event occurs, that object's appropriate method is
	 * invoked.
	 *
	 * @see FilterSelectAllClickEvent
	 */
	private class FilterSelectAllClickListener implements ClickListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button
		 * .ClickEvent)
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			filterField.setValue(filterField.getItemIds());
			filterCheckInvers.setEnabled(false);
			filterCheckInvers.clear();
		}
	}

	/**
	 * The listener interface for receiving filterSelectNoneClick events. The
	 * class that is interested in processing a filterSelectNoneClick event
	 * implements this interface, and the object created with that class is
	 * registered with a component using the component's
	 * <code>addFilterSelectNoneClickListener<code> method. When the
	 * filterSelectNoneClick event occurs, that object's appropriate method is
	 * invoked.
	 *
	 * @see FilterSelectNoneClickEvent
	 */
	private class FilterSelectNoneClickListener implements ClickListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button
		 * .ClickEvent)
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			filterField.clear();
			filterCheckInvers.setEnabled(false);
			filterCheckInvers.clear();
		}
	}

	/**
	 * The listener interface for receiving saveClick events. The class that is
	 * interested in processing a saveClick event implements this interface, and
	 * the object created with that class is registered with a component using
	 * the component's <code>addSaveClickListener<code> method. When the
	 * saveClick event occurs, that object's appropriate method is invoked.
	 *
	 * @see SaveClickEvent
	 */
	private class SaveClickListener implements ClickListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button
		 * .ClickEvent)
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			updateFilterTable();
		}

		/**
		 * Update user account filter table.
		 */
		private void updateFilterTable() {
			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
			criteriaDeleteAndUpdate(em);
			em.getTransaction().commit();
			em.close();
		}

		/**
		 * Criteria delete and update.
		 *
		 * @param em
		 *            the entity manager factory
		 */
		private void criteriaDeleteAndUpdate(EntityManager em) {
			UserGroupDto userGroupDto = (UserGroupDto) userGroupField.getValue();
			CriteriaBuilder cb = em.getCriteriaBuilder();

			// delete all existing entries as prerequisite for the following
			// update
			// of the user account
			CriteriaDelete<Filter> delete = cb.createCriteriaDelete(Filter.class);
			// set the root class
			Root<Filter> userGroupFilterRootDelete = delete.from(Filter.class);
			Join<Filter, UserGroup> uaJoin = userGroupFilterRootDelete.join("userGroup",
					JoinType.LEFT);
			// set where clause
			Predicate cbEqualId = cb.equal(uaJoin.get("id"), userGroupDto.getId());
			String likeString = EntityUtils.getFQNForLEntity((LEntity) entityField.getValue()) + FILTER_DELIMITER + "%";
			Predicate cbLikeEntity = cb.like(userGroupFilterRootDelete.get("filter"), likeString);
			delete.where(cb.and(cbEqualId, cbLikeEntity));
			// perform delete
			em.createQuery(delete).executeUpdate();

			// create an empty userAccount and set only the id.
			UserGroupDtoMapper<UserGroupDto, UserGroup> userGroupMapper = new UserGroupDtoMapper<>();
			UserGroup userGroup = userGroupMapper.createEntity();
			userGroup.setId(userGroupDto.getId());

			FilterDtoMapper<FilterDto, Filter> userGroupFilterMapper = new FilterDtoMapper<>();

			// creation of all the required data to insert
			LEntity entityValue = ((LEntity) entityField.getValue());
			EObject container = entityValue.eContainer();
			if (container != null && container instanceof LPackage) {
				LPackage pckg = (LPackage) container;
				String pckgName = pckg.getName();
				String fqn = pckgName + "." + entityValue.getName();
				@SuppressWarnings("unchecked")
				Collection<IDto> keys = (Collection<IDto>) filterField.getValue();

				// add new entries for this user
				for (IDto dto : keys) {
					Object idValue = DtoUtils.getValue(dto, entityValue.getPrimaryKeyAttribute().getName());
					String filterValue = fqn + FILTER_DELIMITER + idValue;
					// create userAccountFilter entity
					Filter userGroupFilter = userGroupFilterMapper.createEntity();
					userGroupFilter.setId(java.util.UUID.randomUUID().toString());
					userGroupFilter.setFilter(filterValue);
					userGroupFilter.setInvers(filterCheckInvers.getValue());
					userGroupFilter.setUserGroup(userGroup);
					// persist the userAccountFilter entity
					em.persist(userGroupFilter);
				}
				Notification.show("SAVED", "The " + keys.size() + " filter were successfully saved.",
						Notification.Type.HUMANIZED_MESSAGE);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data
	 * .Property.ValueChangeEvent)
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		save.setEnabled(userGroupField.getValue() != null && entityField.getValue() != null && (filterField.getValue() != null));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ui.api.contextfunction.IUserFilterDialogProvider#
	 * getDialog()
	 */
	@Override
	public Component getDialog() {
		VerticalLayout content = new VerticalLayout();
		createView(content);
		return content;
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		emf = persistenceService.getEntityManagerFactory(persistenceId);
	}

	/**
	 * Creates the view.
	 *
	 * @param parent
	 *            the parent
	 */
	@SuppressWarnings("serial")
	@Override
	public void createView(VerticalLayout parent) {
		parent.setSpacing(true);
		parent.setMargin(true);
		parent.setSizeFull();
		parent.setPrimaryStyleName("osbp");

		HorizontalLayout comboArea = new HorizontalLayout();
		comboArea.addStyleName("userFilterComboArea");
		comboArea.setSizeFull();
		comboArea.setMargin(true);
		parent.addComponent(comboArea);

		HorizontalLayout dataArea = new HorizontalLayout();
		dataArea.addStyleName("userFilterDataArea");
		dataArea.setSizeFull();
		dataArea.setMargin(true);
		parent.addComponent(dataArea);

		userGroupField = new ComboBox();
		userGroupField.setItemCaptionPropertyId("userGroupName");
		userGroupField.setImmediate(true);
		userGroupField.setSizeFull();
		comboArea.addComponent(userGroupField);

		entityField = new EObjectComboBox(modelingContext.getAdapterFactory());
		entityField.setImmediate(true);
		entityField.setSizeFull();
		comboArea.addComponent(entityField);

		HorizontalLayout buttonArea = new HorizontalLayout();
		buttonArea.addStyleName("userFilterButtonArea");
		buttonArea.setSizeFull();
		buttonArea.setMargin(true);
		parent.addComponent(buttonArea);

		filterCheckInvers = new CheckBox();
		filterCheckInvers.setImmediate(true);
		filterCheckInvers.setEnabled(false);
		buttonArea.addComponent(filterCheckInvers);
		filterSelectAll = new Button("all");
		filterSelectAll.setSizeFull();
		filterSelectAll.setImmediate(true);
		filterSelectAll.setEnabled(false);
		buttonArea.addComponent(filterSelectAll);

		filterSelectNone = new Button("none");
		filterSelectNone.setSizeFull();
		filterSelectNone.setImmediate(true);
		filterSelectNone.setEnabled(false);
		buttonArea.addComponent(filterSelectNone);

		save = new Button("save");
		save.setSizeFull();
		save.setImmediate(true);
		save.setEnabled(false);
		buttonArea.addComponent(save);

		filterField = new ListSelect() {
			@Override
			public Collection<?> getVisibleItemIds() {
				if (getContainerDataSource() instanceof IAlreadyLoadedItemIdProvider) {
					return ((IAlreadyLoadedItemIdProvider<?>) getContainerDataSource()).getAlreadyLoadedItemIds();
				}
				return super.getVisibleItemIds();
			}
		};
		filterField.setValue("entry to filter");
		filterField.setImmediate(true);
		filterField.setMultiSelect(true);
		filterField.setSizeFull();
		filterField.setRows(25);
		dataArea.addComponent(filterField);
		fillItems();

		// Listener
		userGroupField.addValueChangeListener(this);
		userGroupField.addValueChangeListener(new UserComboBoxValueChangeListener());
		entityField.addValueChangeListener(this);
		entityField.addValueChangeListener(new EntityComboBoxValueChangeListener());
		filterField.addValueChangeListener(this);
		filterField.addValueChangeListener(new FilterValueChangeListener());
		filterSelectAll.addClickListener(new FilterSelectAllClickListener());
		filterSelectNone.addClickListener(new FilterSelectNoneClickListener());
		save.addClickListener(new SaveClickListener());
		user.addUserLocaleListener(this);
	}

	/**
	 * Fill items.
	 */
	protected void fillItems() {
		fillUsersComboBox();
		fillEntityComboBoxContent();
	}

	/**
	 * Fill users combo box.
	 */
	// ---------- Helper Methods ------------
	private void fillUsersComboBox() {
		IDTOService<UserGroupDto> service = DtoServiceAccess.getService(UserGroupDto.class);

		// getting all existing users.
		// not using directly the UserAccountDtoService via find-Method due to
		// the necessary to use a container
		// therefore we use here the combination of
		// StatefulDelegatingDtoSearchService and
		// BeanServiceLazyLoadingContainer.
		StatefulDelegatingDtoSearchService<UserGroupDto> loadingService = new StatefulDelegatingDtoSearchService<>(
				service, UserGroupDto.class);
		BeanServiceLazyLoadingContainer<UserGroupDto> datasource = new BeanServiceLazyLoadingContainer<>(
				loadingService, UserGroupDto.class, null);
		userGroupField.setContainerDataSource(datasource);
	}

	/**
	 * Fill entity combo box content.
	 */
	private void fillEntityComboBoxContent() {
		List<LEntity> result = new ArrayList<>();
		for (EObject eObj : dslMetadataService.getAll(OSBPEntityPackage.Literals.LENTITY)) {
			LEntity entity = (LEntity) eObj;
			if (!entity.isMappedSuperclass() && !entity.getPersistenceUnit().equals("authentication")
					&& !entity.getPersistenceUnit().equals("blob")) {
				result.add(entity);
			}
		}
		entityField.setCollection(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.vaaclipse.publicapi.authentication.IUser.
	 * UserLocaleListener#localeChanged(java.util.Locale)
	 */
	@Override
	public void localeChanged(Locale locale) {
		userGroupField.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "usergroup_tip"));
		entityField.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "entity_tip"));
		filterCheckInvers.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "invers"));
		filterCheckInvers.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "invers_tip"));
		filterSelectAll.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "all"));
		filterSelectAll.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "all_tip"));
		filterSelectNone.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "none"));
		filterSelectNone.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "none_tip"));
		save.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "save"));
		save.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "save_tip"));
		filterField.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "data_tip"));
	}

	// --------------------------------------------------------

}
