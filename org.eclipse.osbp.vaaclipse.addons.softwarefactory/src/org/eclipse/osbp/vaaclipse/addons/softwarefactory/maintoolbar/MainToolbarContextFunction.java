/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.vaaclipse.addons.softwarefactory.maintoolbar;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.osgi.service.component.annotations.Component;

@Component(service = IContextFunction.class, property = "service.context.key=MainToolbar")
public class MainToolbarContextFunction implements IContextFunction {
	@Override
	public Object compute(final IEclipseContext context, final String contextKey) {
		MApplication application = context.get(MApplication.class);
		IEclipseContext appCtx = application.getContext();
		IMainToolbarProvider provider = ContextInjectionFactory.make(
				MainToolbarProvider.class, appCtx);
		appCtx.set(IMainToolbarProvider.class, provider);
		return provider;
	}
}
