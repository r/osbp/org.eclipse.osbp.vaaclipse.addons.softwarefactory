/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.vaaclipse.addons.softwarefactory.service;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.e4.core.services.translation.TranslationService;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;

public class OSBPTranslationService extends TranslationService {
	@Inject
	private IDSLMetadataService dslMetadataService;
	private static final String KEYBINDING = ".keybinding.";
	
	@Override
	protected String getResourceString(String key, ResourceBundle resourceBundle) {
		String s = key.trim();
		if (s.startsWith("%%", 0))
			s = s.substring(1);

		int ix = s.indexOf(' ');
		String rbKey = ix == -1 ? s : s.substring(0, ix);
		return translate(rbKey.substring(1), null);
	}

	@Override
	public String translate(String i18nKey, String contributorURI) {
		if(i18nKey.contains(KEYBINDING)) {
			int idx = i18nKey.indexOf(KEYBINDING);
			String translationTemp = dslMetadataService.translate(locale.toLanguageTag(), i18nKey.substring(0, idx));
			String binding = i18nKey.substring(idx+KEYBINDING.length());
			List<String> parts = Arrays.asList(binding.split(" "));
			translationTemp += " "+parts.stream().map(p->dslMetadataService.translate(locale.toLanguageTag(), p)).collect(Collectors.joining("+", "(", ")"));
			return translationTemp;
		}
		return(dslMetadataService.translate(super.locale.toLanguageTag(), i18nKey));
	}
}
