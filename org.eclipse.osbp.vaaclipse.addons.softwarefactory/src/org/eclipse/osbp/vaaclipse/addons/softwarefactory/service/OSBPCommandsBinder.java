package org.eclipse.osbp.vaaclipse.addons.softwarefactory.service;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.ui.api.contextfunction.ICommandsProvider;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class OSBPCommandsBinder {
	private static List<ICommandsProvider> commandsProviders = new ArrayList<>();
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger("binder");

	/**
	 * Add command provider.
	 *
	 * @param commandsProvider
	 *            the command provider
	 */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public synchronized void bindCommandsProvider(final ICommandsProvider commandsProvider) {
		addCommandsProvider(commandsProvider);
		LOGGER.debug("CommandsProvider added");
	}

	/**
	 * Remove command provider.
	 *
	 * @param commandsProvider
	 *            the command provider
	 */
	public synchronized void unbindCommandsProvider(final ICommandsProvider commandsProvider) {
		removeCommandsProvider(commandsProvider);
		LOGGER.debug("CommandsProvider removed");
	}

	public static List<ICommandsProvider> getCommandsProviders() {
		return commandsProviders;
	}

	public static void addCommandsProvider(ICommandsProvider commandsProvider) {
		OSBPCommandsBinder.commandsProviders.add(commandsProvider);
	}

	public static void removeCommandsProvider(ICommandsProvider commandsProvider) {
		OSBPCommandsBinder.commandsProviders.remove(commandsProvider);
	}
}
