package org.eclipse.osbp.vaaclipse.addons.softwarefactory.perspective;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanbanEvent;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.KanbanBoardEmbeddable;
import org.eclipse.osbp.ui.api.contextfunction.IViewEmbeddedProvider;
import org.eclipse.osbp.ui.api.e4.IE4Dialog;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.perspective.IPerspectiveProvider;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.xtext.dialogdsl.Dialog;

import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class KanbanView
		implements IUser.UserLocaleListener, IE4Dialog {
	@Inject
	MPart part;

	@Inject
	private IUser user;

	@SuppressWarnings("unused")
	@Inject
	private IEventDispatcher eventDispatcher;

	@Inject
	private IDSLMetadataService dslMetadataService;

	private VerticalLayout parent;
	private transient IEclipseContext context;
	private KanbanBoardEmbeddable kanbanBoard;
	private String dialogEcviewId;
	private Dialog dialog;

	@Inject
	public KanbanView(VerticalLayout parent, IEclipseContext context, MApplication app) {
		this.parent = parent;
		this.context = context;
	}

	@PostConstruct
	public void createView() {
		context.set(IE4Dialog.class, this);
		parent.setPrimaryStyleName("osbp");
		parent.setId("parent");
		parent.setSizeFull();

		EObject eObject = (EObject) part.getTransientData().get(IPerspectiveProvider.KanbanConstants.DTO);
		Class<?> dto = dslMetadataService.getClass(eObject, "dto");
		dialog = (Dialog) part.getTransientData().get(IPerspectiveProvider.KanbanConstants.DIALOG);
		Dialog card = (Dialog) part.getTransientData().get(IPerspectiveProvider.KanbanConstants.CARD);

		dialogEcviewId = dslMetadataService.getECViewId(dialog);
		String cardEcviewId = dslMetadataService.getECViewId(card);

		kanbanBoard = new KanbanBoardEmbeddable(dto, dialogEcviewId, cardEcviewId);
		parent.addComponent(kanbanBoard);
		kanbanBoard.setSizeFull();

		kanbanBoard.setDoubleClickConsumer(this::showDialog);
		kanbanBoard.setStateChangedConsumer(e -> System.out.println("stateChange " + e));

		kanbanBoard.init(user.getLocale());
	}

	private void showDialog(YKanbanEvent event) {
		Window dialogWindow = new Window();
		parent.getUI().addWindow(dialogWindow);
		VerticalLayout dialogParent = new VerticalLayout();
		dialogParent.setSizeFull();
		dialogWindow.setContent(dialogParent);

		dialogWindow.setWidth("650px");
		dialogWindow.setHeight("300px");

		IEclipseContext childContext = context.createChild();
		childContext.set(VerticalLayout.class, dialogParent);

		childContext.set("embeddedDialogModel", dialog);
	    IViewEmbeddedProvider content = ContextInjectionFactory.make(DialogProvider.class, childContext);
//		content.addSaveListener(this::refreshCard);
		content.setEnableManualInput(true);
		content.createView(dialogParent);
		content.createComponents();
		content.setInput(event.dto);

		dialogWindow.center();
	}
	
//	private void refreshCard(Object dto){
//		kanbanBoard.refreshCard(dto);
//	}

	@PostConstruct
	public void activate() {
		user.addUserLocaleListener(this);
	}

	@PreDestroy
	public void deactivate() {
		user.removeUserLocaleListener(this);
	}

	@Focus
	public void setFocus() {
		Component parent = this.parent;
		while (!(parent instanceof Panel) && parent != null) {
			parent = parent.getParent();
		}
		if (parent != null) {
			((Panel) parent).focus();
		}
	}

	@Override
	public void localeChanged(Locale locale) {
		kanbanBoard.setLocale(locale);
		kanbanBoard.getViewContext().setLocale(locale);
	}

	@Override
	public String getStateLabelUUID() {
		// TODO Auto-generated method stub
		return null;
	}
}
