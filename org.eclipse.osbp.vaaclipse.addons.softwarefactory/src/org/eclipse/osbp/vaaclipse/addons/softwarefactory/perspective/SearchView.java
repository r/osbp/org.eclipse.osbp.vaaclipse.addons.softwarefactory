package org.eclipse.osbp.vaaclipse.addons.softwarefactory.perspective;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent.EventDispatcherCommand;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent.EventDispatcherDataTag;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.FilteringComponentEmbeddable;
import org.eclipse.osbp.ui.api.e4.IE4Dialog;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.perspective.IPerspectiveProvider;
import org.eclipse.osbp.ui.api.user.IUser;

import com.vaadin.server.ClientConnector;
import com.vaadin.server.ClientConnector.AttachEvent;
import com.vaadin.server.ClientConnector.DetachEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class SearchView<T>
		implements IUser.UserLocaleListener, ClientConnector.DetachListener, ClientConnector.AttachListener, IE4Dialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	MPart part;

	@Inject
	private IUser user;

	@Inject
	private IEventDispatcher eventDispatcher;

	@Inject
	private IDSLMetadataService dslMetadataService;
	
	private VerticalLayout parent;
	private FilteringComponentEmbeddable<T> filteringComponent;
	private transient IEclipseContext context;

	@Inject
	public SearchView(VerticalLayout parent, IEclipseContext context, MApplication app) {
		this.parent = parent;
		this.context = context;
	}

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void createView() {
		context.set(IE4Dialog.class, this);
		parent.setPrimaryStyleName("osbp");
		parent.setId("parent");
		parent.setSizeFull();
		parent.addAttachListener(this);
		parent.addDetachListener(this);

		EObject eObject = (EObject) part.getTransientData().get(IPerspectiveProvider.FilterConstants.FILTER_DTO_CLASS);
		Class<T> dto = (Class<T>) dslMetadataService.getClass(eObject, "dto");
		int depth = (int) part.getTransientData().get(IPerspectiveProvider.FilterConstants.FILTER_DEPTH);
		int filterCols = (int) part.getTransientData().get(IPerspectiveProvider.FilterConstants.FILTER_COLUMNS);
		filteringComponent = new FilteringComponentEmbeddable<>(dto, depth, filterCols);
		parent.addComponent(filteringComponent);
		filteringComponent.setSizeFull();

		filteringComponent.init(user.getLocale());
		filteringComponent.setAcceptCallback(p -> {
			MPerspective perspective = context.get(MPerspective.class);
			EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, dto.getName(), getClass().getSimpleName());
			evnt.addItem(EventDispatcherDataTag.DTO, p);
			eventDispatcher.sendEvent(evnt);
		});
	}

	@Override
	public void attach(AttachEvent event) {
		user.addUserLocaleListener(this);
	}

	@Override
	public void detach(DetachEvent event) {
		user.removeUserLocaleListener(this);
	}

	@Focus
	public void setFocus() {
		Component parent = this.parent;
		while (!(parent instanceof Panel) && parent != null) {
			parent = parent.getParent();
		}
		if (parent != null) {
			((Panel) parent).focus();
		}
	}

	@Override
	public void localeChanged(Locale locale) {
		filteringComponent.setLocale(locale);
		filteringComponent.getViewContext().setLocale(locale);
	}

	@Override
	public String getStateLabelUUID() {
		// TODO Auto-generated method stub
		return null;
	}
}
