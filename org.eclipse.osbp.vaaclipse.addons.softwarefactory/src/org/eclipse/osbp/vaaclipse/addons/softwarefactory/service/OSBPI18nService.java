/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.service;

import java.util.Locale;

import javax.inject.Inject;

import org.apache.commons.lang.math.NumberUtils;
import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;

/**
 * A simple I18n service implementation.
 */
public class OSBPI18nService extends AbstractDisposable implements II18nService {
	private static final String VALIDATION = "javax.validation";
	@Inject
	private IDSLMetadataService dslMetadataService;

	public OSBPI18nService() {
		super();
	}

	public void setDSLMetadataService(IDSLMetadataService dslMetadataService) {
		this.dslMetadataService = dslMetadataService;
	}
	
	@Override
	public String getValue(String i18nKey, Locale locale) {
		// don't do it with numbers
		if(NumberUtils.isNumber(i18nKey.trim())) {
			return i18nKey;
		}
		if(locale == null || dslMetadataService == null) {
			return i18nKey;
		}
		if(i18nKey.startsWith(VALIDATION)) {
			String translationTemp = dslMetadataService.translate(locale.toLanguageTag(), i18nKey);
			if (translationTemp == null) {
				return "";
			}
			return translationTemp;
		}
		String[] splitI18nKey = i18nKey.split("\\.");
		StringBuilder translation = new StringBuilder();
		int bracketsToClose = 0;
		// LIFO - Therefore starting from the last to the first
		for (int i = splitI18nKey.length - 1; i > -1; i--) {
			String translationTemp = dslMetadataService.translate(locale.toLanguageTag(), splitI18nKey[i]);
			if (translationTemp == null) {
				return "";
			}
			// Nested properties will be translated individually and put together
			if (i < splitI18nKey.length - 1) {
				translation.append("(");
				bracketsToClose++;
			}
			translation.append(translationTemp);
		}
		for (int i = 0; i < bracketsToClose; i++) {
			translation.append(")");
		}
		return translation.toString();
	}

	@Override
	protected void internalDispose() {
		// nothing to do
	}
}
