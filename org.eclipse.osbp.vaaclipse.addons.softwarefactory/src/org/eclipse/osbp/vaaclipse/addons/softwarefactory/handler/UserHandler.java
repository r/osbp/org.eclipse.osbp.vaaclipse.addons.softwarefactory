/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.handler;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.osbp.blob.component.BlobComponent;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent.EventDispatcherCommand;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.eclipse.osbp.ui.api.contextfunction.IUserMenuProvider;
import org.eclipse.osbp.ui.api.customfields.IBlobService;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.vaaclipse.publicapi.authentication.AuthenticationConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.hene.popupbutton.PopupButton;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("all")
public class UserHandler implements IUser.UserLocaleListener, IEventDispatcher.Receiver {
	private final static Logger log = LoggerFactory.getLogger(UserHandler.class);
	@Inject
	private MApplication m_Application;

	@Inject
	private IEventDispatcher eventDispatcher;

	@Inject
	private IEventBroker eventBroker;

	@Inject
	private IUser user;

	@Inject
	private MWindow window;

	@Inject
	private IThemeResourceService themeResourceService;

	/** The metadata service. */
	@Inject
	private IDSLMetadataService dslMetadataService;

	@Inject
	private IBlobService blobService;
	
	@Inject
	@Named("Menu")
	private IUserMenuProvider userMenu;

	private EventHandler closeMenu;

	private PopupButton popup;
	private Label name;
	private Label position;
	private Button logout;

	@PostConstruct
	public void init(final ComponentContainer container) {

		HorizontalLayout form = new HorizontalLayout();
		form.addStyleName("ToolControlMenu");
		form.setMargin(false);

		BlobComponent userImage = new BlobComponent(blobService, user.getProfileImageId(), 2);
		userImage.addStyleName("os-userportrait");
		form.addComponent(userImage);

		VerticalLayout info = new VerticalLayout();
		info.setMargin(false);

		name = new Label(user.getUserName());
		name.addStyleName("os-username");
		info.addComponent(name);
		position = new Label(user.getPosition());
		position.addStyleName("os-userposition");
		info.addComponent(position);

		VerticalLayout buttons = new VerticalLayout();
		buttons.setMargin(false);
		buttons.setSpacing(false);
		form.addComponent(buttons);
		form.addComponent(info);

		logout = new Button(themeResourceService.getThemeResource("logout_big", ThemeResourceType.ICON));
		logout.addStyleName("os-userlogoutbutton");
		logout.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				logoutUser();
			}
		});
		buttons.addComponent(logout);

		if(!ProductConfiguration.hideUserMenu()){
			popup = new PopupButton();
			popup.addStyleName("os-usermenubutton");
			popup.setContent(userMenu.getMenu(popup));
			buttons.addComponent(popup);
		} else {
			log.info("UserMenu is hidden due to prefereces!");
		}

		container.addComponent(form);
		user.addUserLocaleListener(this);
		eventDispatcher.addEventReceiver(this);
	}

	private void logoutUser() {
		log.debug("logout user");
		eventBroker.send(AuthenticationConstants.Events.Authentication.PRE_LOGOUT, new Object());
		eventBroker.send(AuthenticationConstants.Events.Authentication.LOGOUT, new Object());
	}

	@PreDestroy
	private void deactivate() {
		eventDispatcher.removeEventReceiver(this);
	}

	@Override
	public void localeChanged(Locale locale) {
		name.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "userLabelTooltip"));
		position.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "positionLabelTooltip"));
		logout.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "logoutButtonTooltip"));
		if(!ProductConfiguration.hideUserMenu()){		
			popup.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "userMenuTooltip"));
		}
	}

	@Override
	public void receiveEvent(EventDispatcherEvent event) {
		if(event.getTopic().equals("UserMenu") && event.getCommand() == EventDispatcherCommand.CLOSE) {
			popup.setPopupVisible(false);
		}
	}
}
