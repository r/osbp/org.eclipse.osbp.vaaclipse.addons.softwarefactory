/*
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                               
 * 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.systemsettings;

import java.util.Locale;

import javax.inject.Inject;

import org.eclipse.osbp.core.api.persistence.IPersistenceService;
import org.eclipse.osbp.ui.api.contextfunction.IVaadinDialogProvider;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class UserFilterDialogProvider enables to inject a filter on runtime data
 * into the dto service.
 */
public class SystemSettingsDialogProvider implements IVaadinDialogProvider, IUser.UserLocaleListener {
	/** The persistence service. */
	@Inject
	private IPersistenceService persistenceService;

	/** The modeling context. */
	@Inject
	IModelingContext modelingContext;

	/** The reset for mondrian cache. */
	private Button resetMondrianCache;

	/** The reset for jpa cache. */
	private Button resetJpaCache;

	/** The reset for user properties. */
	private Button resetUserProperties;

	/** The metadata service. */
	@Inject
	private IDSLMetadataService dslMetadataService;

	/** The user. */
	@Inject
	private IUser user;

	/** The button panel. */
	private Panel buttonPanel;

	/**
	 * The listener interface for receiving resetMondrianCacheClick events. The
	 * class that is interested in processing a resetMondrianCacheClick event
	 * implements this interface, and the object created with that class is
	 * registered with a component using the component's
	 * <code>addResetMondrianCacheClickListener<code> method. When the
	 * resetMondrianCacheClick event occurs, that object's appropriate method is
	 * invoked.
	 *
	 * @see ResetMondrianCacheClickEvent
	 */
	private class ResetMondrianCacheClickListener implements ClickListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button
		 * .ClickEvent)
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			persistenceService.clearMondrianCache(); 
		}
	}

	/**
	 * The listener interface for receiving resetJpaCacheClick events. The
	 * class that is interested in processing a resetJpaCacheClick event
	 * implements this interface, and the object created with that class is
	 * registered with a component using the component's
	 * <code>addResetJpaCacheClickListener<code> method. When the
	 * resetJpaCacheClick event occurs, that object's appropriate method is
	 * invoked.
	 *
	 * @see ResetJpaCacheClickEvent
	 */
	private class ResetJpaCacheClickListener implements ClickListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button
		 * .ClickEvent)
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			persistenceService.clearJpaCache(); 
		}
	}

	/**
	 * The listener interface for receiving resetUserPropertiesClick events. The
	 * class that is interested in processing a resetUserPropertiesClick event
	 * implements this interface, and the object created with that class is
	 * registered with a component using the component's
	 * <code>addResetUserPropertiesClickListener<code> method. When the
	 * resetUserPropertiesClick event occurs, that object's appropriate method is
	 * invoked.
	 *
	 * @see ResetUserPropertiesClickEvent
	 */
	private class ResetUserPropertiesClickListener implements ClickListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button
		 * .ClickEvent)
		 */
		@Override
		public void buttonClick(ClickEvent event) {
			user.clearFromProperties();
			user.persistProperties();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ui.api.contextfunction.IUserFilterDialogProvider#
	 * getDialog()
	 */
	@Override
	public Component getDialog() {
		VerticalLayout content = new VerticalLayout();
		createView(content);
		return content;
	}

	/**
	 * Creates the view.
	 *
	 * @param parent
	 *            the parent
	 */
	@Override
	public void createView(VerticalLayout parent) {
		parent.setSpacing(true);
		parent.setMargin(true);
		parent.setSizeFull();
		parent.setPrimaryStyleName("osbp");

		HorizontalLayout area = new HorizontalLayout();
		area.addStyleName("systemSettingsArea");
		area.setSizeFull();
		parent.setSpacing(true);
		area.setMargin(true);
		area.setSpacing(true);
		parent.addComponent(area);
		
		buttonPanel = new Panel("reset");
		buttonPanel.addStyleName("os-group-panel");
		area.addComponent(buttonPanel);
		HorizontalLayout buttonArea = new HorizontalLayout();
		buttonArea.setSizeFull();
		buttonArea.setMargin(true);
		buttonArea.setSpacing(true);
		buttonPanel.setContent(buttonArea);

		resetUserProperties = new Button("reset_user_properties");
		resetUserProperties.setSizeFull();
		resetUserProperties.setImmediate(true);
		resetUserProperties.setEnabled(true);
		buttonArea.addComponent(resetUserProperties);
		
		resetMondrianCache = new Button("reset_mondrian_cache");
		resetMondrianCache.setSizeFull();
		resetMondrianCache.setImmediate(true);
		resetMondrianCache.setEnabled(true);
		buttonArea.addComponent(resetMondrianCache);

		resetJpaCache = new Button("reset_jpa_cache");
		resetJpaCache.setSizeFull();
		resetJpaCache.setImmediate(true);
		resetJpaCache.setEnabled(true);
		buttonArea.addComponent(resetJpaCache);

		// Listener
		resetUserProperties.addClickListener(new ResetUserPropertiesClickListener());
		resetMondrianCache.addClickListener(new ResetMondrianCacheClickListener());
		resetJpaCache.addClickListener(new ResetJpaCacheClickListener());
		user.addUserLocaleListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.vaaclipse.publicapi.authentication.IUser.
	 * UserLocaleListener#localeChanged(java.util.Locale)
	 */
	@Override
	public void localeChanged(Locale locale) {
		buttonPanel.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "resetcache"));
		resetUserProperties.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "reset_user_properties"));
		resetUserProperties.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "reset_user_properties"));
		resetUserProperties.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "reset_user_properties_tip"));
		resetMondrianCache.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "reset_mondrian_cache"));
		resetMondrianCache.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "reset_mondrian_cache_tip"));
		resetJpaCache.setCaption(dslMetadataService.translate(locale.toLanguageTag(), "reset_jpa_cache"));
		resetJpaCache.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "reset_jpa_cache_tip"));
	}

	// --------------------------------------------------------

}
