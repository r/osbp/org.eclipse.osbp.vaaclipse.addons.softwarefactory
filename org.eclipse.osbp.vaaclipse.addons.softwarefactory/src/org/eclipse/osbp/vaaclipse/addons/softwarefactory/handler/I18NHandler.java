/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.handler;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.osbp.preferences.LocaleUtils;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.themes.EnumCssClass;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType;
import org.eclipse.osbp.ui.api.user.IUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.UI;

@SuppressWarnings("all")
public class I18NHandler implements IUser.UserLocaleListener {
	private final static Logger log = LoggerFactory.getLogger("change locale");
	@Inject
	private IThemeResourceService themeResourceService;

	@Inject
	private IUser user;

	/** The metadata service. */
	@Inject
	private IDSLMetadataService dslMetadataService;

	private ComboBox localeCombo;
	private boolean noEvent = true;

	@PostConstruct
	public void init(final ComponentContainer container) {
		FormLayout form = new FormLayout();
		form.addStyleName("ToolControlCombo");
		form.setMargin(false);
		form.setSpacing(false);
		localeCombo = new ComboBox();
		localeCombo.addStyleName(EnumCssClass.COMBOBOX_WITH_ICON.styleName());
		localeCombo.setItemCaptionMode(ItemCaptionMode.EXPLICIT_DEFAULTS_ID);
		localeCombo.setNullSelectionAllowed(false);
		localeCombo.setWidth("200px");
		localeCombo.setTextInputAllowed(false);
		boolean needCountry = false;
		if (UI.getCurrent().getLocale().getCountry().length() == 0) {
			needCountry = true;
		}
		Locale[] locales = Locale.getAvailableLocales();
		// --- sort by display name used in the respective locale, if it is
		// configured ---
		LocaleUtils.sortLocalesOnDisplayName(locales);
		for (Locale locale : locales) {
			// only allow locales with country due to currency sign formatters
			if (locale.getCountry().length() > 0) {
				String loc = locale.toLanguageTag();
				if (needCountry && locale.getLanguage().equals(UI.getCurrent().getLocale().getLanguage())) {
					// force a default country if not already given by default
					Locale newLocale = new Locale(locale.getLanguage(), locale.getCountry());
					UI.getCurrent().setLocale(newLocale);
					Locale.setDefault(newLocale);
					needCountry = false;
				}
				String path = "";
				if (ProductConfiguration.getLanguages().containsKey(locale.getLanguage()) /*
																						 * && ProductConfiguration.getLanguages () .get (locale.getLanguage()).equals (locale
																						 * .getCountry())
																						 */) {
					if (log.isDebugEnabled())
						log.debug("language preference:" + locale.getLanguage() + " " + locale.getCountry() + " " + locale.getVariant());
					try {
						// --- set the display name used in the respective
						// locale, if it is configured ---
						localeCombo.setItemCaption(loc, LocaleUtils.getDisplayName(locale));
						localeCombo.addItem(loc);
						if (themeResourceService != null) {
							localeCombo.setItemIcon(loc, themeResourceService.getThemeResource(locale.getCountry().toLowerCase(), ThemeResourceType.FLAG));
						}
					} catch (Exception e) {
					}
				}
			}
		}
		form.addComponent(localeCombo);
		container.addComponent(form);
		localeCombo.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (!noEvent) {
					if (log.isDebugEnabled())
						log.debug("will set locale to:" + event.getProperty().getValue());
					user.setLocale(Locale.forLanguageTag((String) event.getProperty().getValue()));
				}
			}
		});
		if (log.isDebugEnabled())
			log.debug("default locale:" + user.getLocale().toLanguageTag());
		localeCombo.select(user.getLocale().toLanguageTag());
		noEvent = false;
		user.addUserLocaleListener(this);
	}

	@Override
	public void localeChanged(Locale locale) {
		localeCombo.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "localeTooltip"));
	}
}
