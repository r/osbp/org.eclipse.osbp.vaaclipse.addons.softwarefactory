/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.maintoolbar;

import org.eclipse.osbp.xtext.action.ActionToolbar;

// TODO: Auto-generated Javadoc
/**
 * The Interface IMainToolbarProvider.
 */
public interface IMainToolbarProvider {
	
	/**
	 * Adds the toolbar.
	 *
	 * @param perspectiveId the perspective id
	 * @param toolBarModel the tool bar model
	 */
	void addToolbar(String perspectiveId, ActionToolbar toolBarModel);
	
	/**
	 * Removes the toolbar.
	 *
	 * @param perspectiveId the perspective id
	 */
	void removeToolbar(String perspectiveId);
	
	/**
	 * Sets the toolbar active or inactive.
	 *
	 * @param perspectiveId the perspective id
	 * @param active the active
	 */
	void setActive(String perspectiveId, boolean active);
	
	/**
	 * Gets the unique toolbar id composed of action id and uuid.
	 *
	 * @return the toolbar id
	 */
	String getToolbarId();
}
