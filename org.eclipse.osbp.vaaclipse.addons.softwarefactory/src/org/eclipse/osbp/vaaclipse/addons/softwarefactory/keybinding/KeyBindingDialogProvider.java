/*
 *                                                                            
 *  Copyright (c) 2011 - 2017 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                              
 * 
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory.keybinding;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.e4.ui.model.application.commands.MBindingContext;
import org.eclipse.e4.ui.model.application.commands.MBindingTable;
import org.eclipse.e4.ui.model.application.commands.MCommand;
import org.eclipse.e4.ui.model.application.commands.MKeyBinding;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ui.api.contextfunction.IVaadinDialogProvider;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.vaaclipse.addons.keybinding.IKeyBindingService;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class KeyBindingDialogProvider.
 */
public class KeyBindingDialogProvider implements IVaadinDialogProvider, IUser.UserLocaleListener {
	/** The metadata service. */
	@Inject
	private IDSLMetadataService dslMetadataService;

	/** The user. */
	@Inject
	private IUser user;

	/** The keybinding service. */
	@Inject
	private IKeyBindingService keybindingService;

	/** The table. */
	private Table table;

	/** The container. */
	private BeanItemContainer<BindingBean> container;

	/** The locale. */
	private Locale locale;

	@Override
	public void localeChanged(Locale locale) {
		this.locale = locale;
		table.setColumnHeader("name", dslMetadataService.translate(locale.toLanguageTag(), "keybindingName"));
		table.setColumnHeader("description", dslMetadataService.translate(locale.toLanguageTag(), "keybindingDescription"));
		table.setColumnHeader("keySequence", dslMetadataService.translate(locale.toLanguageTag(), "keybindingKeySequence"));
		
		container.removeAllItems();
		List<MKeyBinding> bindings = keybindingService.getAllKeyBindings();
		for (MKeyBinding binding : bindings) {
			container.addBean(new BindingBean(binding, dslMetadataService, locale));
		}
	}

	@Override
	public Component getDialog() {
		VerticalLayout content = new VerticalLayout();
		createView(content);
		return content;
	}

	@Override
	public void createView(VerticalLayout parent) {
		locale = user.getLocale();
		table = new Table();
		table.setSelectable(true);
		table.setSizeFull();
		parent.addComponent(table);

		container = new BeanItemContainer<>(BindingBean.class);
		table.setContainerDataSource(container);

		List<MKeyBinding> bindings = keybindingService.getAllKeyBindings();
		for (MKeyBinding binding : bindings) {
			container.addBean(new BindingBean(binding, dslMetadataService, locale));
		}

		table.setVisibleColumns(new Object[] { "name", "description", "keySequence"});
		user.addUserLocaleListener(this);
	}

	/**
	 * The Class BindingBean.
	 */
	public static class BindingBean {

		/** The binding. */
		private MKeyBinding binding;
		
		/** The binding table. */
		private MBindingTable bindingTable;
		
		/** The binding context. */
		private MBindingContext bindingContext;
		
		/** The command. */
		private MCommand command;

		private IDSLMetadataService dslMetadataService;

		private Locale locale;

		/**
		 * Instantiates a new binding bean.
		 *
		 * @param binding
		 *            the binding
		 */
		public BindingBean(MKeyBinding binding, IDSLMetadataService dslMetadataService, Locale locale) {
			super();
			this.binding = binding;
			this.dslMetadataService = dslMetadataService;
			this.locale = locale;
			this.command = binding.getCommand();
			this.bindingTable = (MBindingTable) ((EObject) binding)
					.eContainer();
			this.bindingContext = bindingTable.getBindingContext();
		}

		/**
		 * Gets the description.
		 *
		 * @return the description
		 * @throws NotDefinedException
		 *             the not defined exception
		 */
		public final String getDescription() throws NotDefinedException {
			return dslMetadataService.translate(locale.toLanguageTag(), command.getDescription());
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 * @throws NotDefinedException
		 *             the not defined exception
		 */
		public final String getName() throws NotDefinedException {
			return command.getCommandName();
		}

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public final String getId() {
			return command.getElementId();
		}

		/**
		 * Gets the category.
		 *
		 * @return the category
		 * @throws NotDefinedException
		 *             the not defined exception
		 */
		public final String getCategory() throws NotDefinedException {
			try {
				return command.getCategory().getName();
			} catch (Exception e) {
				return "";
			}
		}

		/**
		 * Gets the key sequence.
		 *
		 * @return the key sequence
		 */
		public final String getKeySequence() {
			return binding.getKeySequence();
		}

		/**
		 * Gets the context name.
		 *
		 * @return the context name
		 */
		public final String getContextName() {
			return bindingContext.getName();
		}

		/**
		 * Gets the context description.
		 *
		 * @return the context description
		 */
		public final String getContextDescription() {
			return bindingContext.getDescription();
		}

	}

}
