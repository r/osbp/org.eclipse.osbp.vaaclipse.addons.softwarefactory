/*
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                               
 * 
 */

/**
 * 
 *  Additions to the vaaclipse environment. 
 *  Provides basic service injections for translations, perspectives and others. The OSBPServiceAddon must therefore be present 
 *  in the Add-on section of Application.xmi.
 *  Provides toolControl handler for the user menu, language, layouting and focusing strategy and theme selection.
 *  Provides BPM task client handler to be notified about BPM task completitions and to trigger BPM tasks
 *  Provides data filtering on DTO basis and its settings dialogs for super-users.
 *  Provides main menu tool-bar functions to be injected in perspectives, tables, dialogs and others.
 *  Provides the welcome perspective and its content
 *   
 *  @since  0.9.0
 *  
 *  <p>The Class Dependency Graph<br>
 *  <img src="doc-files/model.png" alt="The Class Dependency Graph">
 */
package org.eclipse.osbp.vaaclipse.addons.softwarefactory;



