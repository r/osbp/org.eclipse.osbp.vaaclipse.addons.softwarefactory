package org.eclipse.osbp.vaaclipse.addons.softwarefactory.problems;

import java.util.Locale;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.user.IUser;

import com.vaadin.server.ClientConnector.DetachListener;
import com.vaadin.ui.Window;

public class ProblemsPopup extends Window implements DetachListener, IUser.UserLocaleListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1106131179854247707L;
	private ProblemsViewContent problemsViewContent;
	private transient IDSLMetadataService dslMetadataService;

	public ProblemsPopup(IEclipseContext eclipseContext, IUser user) {
		super();
		dslMetadataService = eclipseContext.get(IDSLMetadataService.class);
		super.setLocale(user.getLocale());
		this.addDetachListener(this);
		setClosable(true);
		setModal(false);
		setCaption(dslMetadataService.translate(user.getLocale().toLanguageTag(), "validationReport"));
		problemsViewContent = new ProblemsViewContent();
		setContent(problemsViewContent.createComponents(eclipseContext, user.getLocale(), this));
		user.addUserLocaleListener(this);
	}

	@Override
	public void detach(DetachEvent event) {
		problemsViewContent.setVisible(false);
	}

	@Override
	public void localeChanged(Locale locale) {
		super.setLocale(locale);
		setCaption(dslMetadataService.translate(locale.toLanguageTag(), "validationReport"));
		if(problemsViewContent != null) {
			problemsViewContent.setLocale(locale);
		}
	}
}
